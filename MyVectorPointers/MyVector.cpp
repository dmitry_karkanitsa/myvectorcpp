#include "MyVector.h";

template MyVector<int>;
template MyVector<double>;
template MyVector<float>;
template MyVector<char>;
template MyVector<bool>;

template<typename T>
MyVector<T>::MyVector(unsigned int n)
{
	if (n != 0) {
		first = all.allocate(n * coeff_capacity);
		for (int i = 0; i < n; i++)
		{
			all.construct(first + i, T());
		}
		last = &first[n];
		end = &first[int(n * coeff_capacity)];
	}
	else {
		first = all.allocate(n);
		last = nullptr;
		end = nullptr;
	}
	correct = true;
}

template<typename T>
MyVector<T>::MyVector(unsigned int n, T value)
{
	first = all.allocate(n * coeff_capacity);
	for (int i = 0; i < n; i++)
	{
		all.construct(first + i, value);
	}
	last = &(first[n]);
	end = &(first[int(n * coeff_capacity)]);
	correct = true;
}

template<typename T>
MyVector<T>::MyVector(MyVector<T>& vector)
{
	first = all.allocate(vector.Size() * coeff_capacity);
	for (int i = 0; i < vector.Size(); i++)
	{
		all.construct(first + i, vector[i]);
	}
	last = &(first[vector.Size()]);
	end = &(first[int(vector.Size() * coeff_capacity)]);
	correct = true;
}

template<typename T>
MyVector<T>::MyVector(std::initializer_list<T> list)
{
	first = all.allocate(list.size() * coeff_capacity);
	int k = 0;
	for (auto i = list.begin(); i != list.end(); i++)
	{
		all.construct(first + k, *i);
		k++;
	}
	last = &(first[list.size()]);
	end = &first[int(list.size() * coeff_capacity)];
	correct = true;
}

template<typename T>
MyVector<T>::~MyVector()
{
	for (int i = 0; i < last - first; i++)
	{
		all.destroy(first + i);
	}
	all.deallocate(first, end - first);	
}

template<typename T>
int MyVector<T>::Size()
{
	correct = true;
	return (last - first);
}

template<typename T>
int MyVector<T>::Capacity() {
	correct = true;
	return (end - first);
}

template<typename T>
void MyVector<T>::Clear()
{
	for (int i = 0; i < last - first; i++)
	{
		all.destroy(first + i);
	}
	all.deallocate(first, end - first);
	last = nullptr;
	end = nullptr;
	first = nullptr;
	correct = true;
}

template<typename T>
void MyVector<T>::Push_back(T value)
{
	if (last == nullptr && end == nullptr)
	{
		first = all.allocate(1);
		first[0] = value;
		end = last = &(first[1]);
	}
	else if (end == last) {
		int sz = (last - first + 1) * coeff_capacity;
		T* arr1 = all.allocate(sz);
		for (int i = 0; i < (last - first); i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		all.construct(arr1 + (last - first), value);
		int n = last - first;
		all.deallocate(first, end - first);
		first = arr1;
		last = &(first[n + 1]);
		end = &(first[int((n + 1) * coeff_capacity)]);
	}
	else {
		all.construct(first + (last - first), value);
		last = &(first[last - first + 1]);
	}
	correct = true;
}

template<typename T>
void MyVector<T>::Erase(int position)
{
	if (position < 0 || position >= last - first)
	{
		correct = false;
		return;
	}
	int sz = (last - first - 1);
	T* arr1 = all.allocate(sz * coeff_capacity);
	for (int i = 0; i < position; i++)
	{
		all.construct(arr1 + i, first[i]);
		all.destroy(first + i);
	}
	for (int i = position + 1; i < sz + 1; i++)
	{
		all.construct(arr1 + i - 1, first[i]);
		all.destroy(first + i);
	}
	all.deallocate(first, end - first);
	first = arr1;
	last = &(first[sz]);
	end = &(first[int(sz * coeff_capacity)]);
	correct = true;
}

template<typename T>
void MyVector<T>::Assign(int count, T value)
{
	if (count < 0)
	{
		correct = false;
		return;
	}
	T* arr1 = all.allocate(count * coeff_capacity);
	for (int i = 0; i < count; i++)
	{
		all.construct(arr1 + i, value);
	}
	for (int i = 0; i < last - first; i++)
	{
		all.destroy(first + i);
	}
	all.deallocate(first, end - first);
	first = arr1;
	last = &(first[count]);
	end = &(first[int(count * coeff_capacity)]);
	correct = true;
}

template<typename T>
T MyVector<T>::At(int index)
{
	if (last - first > 0 && index >= 0 && index < last - first)
	{
		correct = true;
		return first[index];
	}
	correct = false;
	return T();
}

template<typename T>
T MyVector<T>::Front() {
	if (last - first > 0)
	{
		correct = true;
		return *first;
	}
	correct = false;;
	return T();
}

template<typename T>
T MyVector<T>::Back() {
	if (last - first > 0) {
		return *(first + (last - first) - 1);
		correct = true;
	}
	correct = false;
	return T();
}

template<typename T>
void MyVector<T>::Shrink_to_fit() {
	int sz = last - first;
	T* arr1 = all.allocate(sz);
	for (int i = 0; i < sz; i++)
	{
		all.construct(arr1 + i, first[i]);
		all.destroy(first + i);
	}
	all.deallocate(first, end - first);
	first = arr1;
	last = end = &(first[sz]);
	correct = true;
}

template<typename T>
void MyVector<T>::Resize( int count, T value)
{
	if (count < 0)
	{
		correct = false;
		return;
	}
	if (last - first < count)
	{
		T* arr1 = all.allocate(count * coeff_capacity);
		for (int i = 0; i < last - first; i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		for (int i = last - first; i < count; i++)
		{
			all.construct(arr1 + i, value);
		}
		all.deallocate(first, end - first);
		first = arr1;
		last = &(first[count]);
		end = &(first[int(count * coeff_capacity)]);
	}
	else
	{
		T* arr1 = all.allocate(count * coeff_capacity);
		for (int i = 0; i < count; i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		all.deallocate(first, end - first);
		first = arr1;
		last = &(first[count]);
		end = &(first[int(count * coeff_capacity)]);
	}
	correct = true;
}

template<typename T>
void MyVector<T>::Pop_back() {
	if (last == first)
	{
		correct = false;
		return;
	}
	all.destroy(first + (last - first - 1));
	all.construct(first + (last - first - 1), T());
	last--;
	correct = true;
}

template<typename T>
void MyVector<T>::Reserve(unsigned int sz) {
	if (sz > (end - first))
	{
		T* arr1 = all.allocate(sz);
		int n = last - first;
		for (int i = 0; i < last - first; i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		all.deallocate(first, end - first);
		first = arr1;
		last = &(first[n]);
		end = &(first[sz]);
	}
	correct = true;
}

template<typename T>
bool MyVector<T>::Empty() {
	correct = true;
	return (first == last);
}

template<typename T>
T* MyVector<T>::Data() {
	if (first == nullptr) {
		correct = false;
	}
	else {
		correct = true;
	}
	return first;
}

template<typename T>
int MyVector<T>::Max_Size() {
	correct = true;
	return all.max_size();
}

template<typename T>
allocator<T> MyVector<T>::Get_allocator() {
	correct = true;
	return all;
}

template<typename T>
void MyVector<T>::Sort() {
}

template<typename T>
int MyVector<T>::Binary_search(T value) {
	correct = true;
	int left = 0;
	int right = (last - first) - 1;
	int middle = 0;
	while (left <= right)
	{
		middle = (right + left) / 2;
		if (first[middle] == value)
		{
			return middle;
		}
		if (value < first[middle])
		{
			right = middle - 1;
		}
		else
		{
			left = middle + 1;
		}
	}
	return -1;
}

template <typename T>
void MyVector<T>::Insertion_sort() {
	for (int i = 1; i < last - first; i++) {
		int j = i;
		while (j > 0 && first[j] < first[j - 1]) {
			swap(first[j], first[j - 1]);
			j--;
		}
	}
	correct = true;
}

template<typename T>
bool MyVector<T>::Correct() {
	return correct;
}

