#pragma once
#include <memory>
#include<iostream>
using namespace std;

template<class T>
class MyVector {
public:
	//consructors
	MyVector<T>(unsigned int n, T value);
	MyVector<T>(unsigned int n = 0);
	MyVector<T>(MyVector<T>& vector);
	MyVector<T>(std::initializer_list<T> list);
	~MyVector<T>();
	//consructors

	//operators	
	MyVector& operator = (const MyVector& vector) {
		T* arr1 = all.allocate((vector.last - vector.first) * coeff_capacity);
		for (int i = 0; i < vector.last - vector.first; i++)
		{
			all.construct(arr1 + i, vector.first[i]);
			all.destroy(vector.first + i);
			arr1[i] = vector.first[i];
		}
		all.deallocate(first, end - first);
		first = arr1;
		last = &(vector.first[vector.last - vector.first]);
		end = &(vector.first[int((vector.last - vector.first) * coeff_capacity)]);
		return *this;
	}
	bool      operator ==(const MyVector& vector)
	{
		if (last - first != vector.last - vector.first)
		{
			return false;
		}
		else {
			for (int i = 0; i < vector.last - vector.first; i++)
			{
				if (first[i] != vector.first[i])
				{
					return false;
				}
			}
		}
		return true;
	}
	bool      operator !=(const MyVector& vector)
	{
		if (last - first != vector.last - vector.first)
		{
			return true;
		}
		else {
			for (int i = 0; i < last - first; i++)
			{
				if (first[i] != vector.first[i])
				{
					return true;
				}
			}
		}
		return false;
	}
	bool      operator < (const MyVector& vector)
	{
		if (last - first >= vector.last - vector.first)
		{
			for (int i = 0; i < vector.last - vector.first; i++)
			{
				if (first[i] < vector.first[i])
				{
					return true;
				}
				else if (first[i] > vector.first[i])
				{
					return false;
				}
			}
			return false;
		}
		else
		{
			for (int i = 0; i < last - first; i++)
			{
				if (first[i] < vector.first[i])
				{
					return true;
				}
				else if (first[i] > vector.first[i])
				{
					return false;
				}
			}
			return true;
		}
	}
	bool	  operator > (const MyVector& vector)
	{
		if (last - first > vector.last - vector.first)
		{
			for (int i = 0; i < vector.last - vector.first; i++)
			{
				if (first[i] > vector.first[i])
				{
					return true;
				}
				else if (first[i] < vector.first[i])
				{
					return false;
				}
			}
			return true;
		}
		else
		{
			for (int i = 0; i < last - first; i++)
			{
				if (first[i] > vector.first[i])
				{
					return true;
				}
				else if (first[i] < vector.first[i])
				{
					return false;
				}
			}
			return false;
		}
	}
	bool	  operator <=(const MyVector& vector)
	{
		if (last - first <= vector.last - vector.first)
		{
			for (int i = 0; i < last - first; i++)
			{
				if (first[i] < vector.first[i])
				{
					return true;
				}
				else if (first[i] > vector.first[i])
				{
					return false;
				}
			}
			return true;
		}
		else
		{
			for (int i = 0; i < vector.last - vector.first; i++)
			{
				if (first[i] < vector.first[i])
				{
					return true;
				}
				else if (first[i] > vector.first[i])
				{
					return false;
				}
			}
			return false;
		}
	}
	bool	  operator >=(const MyVector& vector)
	{
		if (last - first >= vector.last - vector.first)
		{
			for (int i = 0; i < vector.last - vector.first; ++i)
			{
				if (first[i] > vector.first[i])
				{
					return true;
				}
				else if (first[i] < vector.first[i])
				{
					return false;
				}
			}
			return true;
		}
		else {
			for (int i = 0; i < vector.last - vector.first; ++i)
			{
				if (first[i] > vector.first[i])
				{
					return true;
				}
				else if (first[i] < vector.first[i])
				{
					return false;
				}
			}
			return false;
		}
	}
	T& operator [](int index)
	{
		if (last - first > 0 && index >= 0 && index < last - first)
		{
			correct = true;
			return first[index];
		}		
		else if (index >= last - first)
		{
			correct = false;
			T* arr1 = all.allocate((index + 1) * coeff_capacity);
			for (int i = 0; i < last - first; i++)
			{
				all.construct(arr1 + i, first[i]);
				all.destroy(first + i);
			}
			for (int i = last - first; i < index + 1; i++)
			{
				all.construct(arr1 + i, T());
			}
			all.deallocate(first, end - first);
			first = arr1;
			last = &first[index + 1];
			end = &first[int((index + 1)*coeff_capacity)];
			return first[index];
		}
		else if(index <= 0 && last - first == 0 ) {
			correct = false;
			T* arr1 = all.allocate(1);
			arr1[0] = T();
			first = arr1;
			last = &first[1];
			end = &first[1];
			return first[0];
		}
		else {
			correct = false;
			return first[0];
		}
	}
	//operators

	//methods
	bool		 Empty();
	int			 Size();
	int			 Max_Size();
	int			 Capacity();
	void		 Clear();
	void		 Push_back(T value);
	void		 Resize(int count, T value = T());
	void	     Erase(int position);
	void		 Assign(int count, T value);
	void		 Shrink_to_fit();
	void		 Pop_back();
	void		 Reserve(unsigned int sz);
	void		 Swap(MyVector<T>& v)
	{
		int sz2 = v.Size();
		int sz1 = last - first;
		T* arr2 = all.allocate(v.Size() * coeff_capacity);
		T* arr1 = all.allocate((last - first) * coeff_capacity);
		for (int i = 0; i < v.last - v.first; i++)
		{
			all.construct(arr2 + i, v[i]);
			all.destroy(v.first + i);
		}
		all.deallocate(v.first, v.end - v.first);
		
		for (int i = 0; i < last - first; i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		all.deallocate(first, end - first);

		v.first = arr1;
		v.last = &(v.first[sz1]);
		v.end = &(v.first[int(sz1 * coeff_capacity)]);

		first = arr2;
		last = &(first[sz2]);
		end = &(first[int(sz2 * coeff_capacity)]);
		correct = true;
	}
	T At(int index);
	T Front();
	T Back();
	T* Data();
	allocator<T> Get_allocator();
	//methods

	//myMethods
	void Sort();
	void Insertion_sort();
	int Binary_search(T value);
	bool Correct();
	//myMethods

	//classes iterators
	class Iterator
	{
	public:

		Iterator(T* first)
		{
			current = first;
		}
		T* operator+ (int n) { return current + n; }
		T* operator- (int n) { return current - n; }
		T* operator++ (int n) { return current++; }
		T* operator-- (int n) { return current--; }
		T* operator++ () { return ++current; }
		T* operator-- () { return --current; }
		T* operator= (Iterator it) { current = it.current;  return current; }
		int  operator- (Iterator it) { return current - it.current; }
		bool operator==(const Iterator& it) { return current == it.current; }
		bool operator!=(const Iterator& it) { return current != it.current; }
		bool operator>(const Iterator& it) { return current > it.current; }
		bool operator<(const Iterator& it) { return current < it.current; }
		T& operator*() { return *current; }
		T* Get_pointer() {return current; }

	private:
		T* current;
	};
	class Reverse_iterator
	{
	public:
		Reverse_iterator(T* first)
		{
			current = first;
		}
		T* operator+ (int n) { return current - n; }
		T* operator- (int n) { return current + n; }
		T* operator++ (int n) { return current--; }
		T* operator-- (int n) { return current++; }
		T* operator++ () { return --current; }
		T* operator-- () { return ++current; }
		T* operator= (Reverse_iterator it) { current = it.current;  return current; }
		bool operator==(const Reverse_iterator& it) { return current == it.current; }
		bool operator!=(const Reverse_iterator& it) { return current != it.current; }
		T& operator*() { return *current; }
	private:
		T* current;
	};
	//classes iterators

	//iterators
	Iterator         Begin() { return first; };
	Iterator         End() { return last; }
	Iterator         Insert(Iterator pos, const T& value)
	{		
		if (pos.Get_pointer() >= last || pos.Get_pointer() < first)
		{
			correct = false;
			return pos;
		}
		int sz = last - first + 1;
		T* arr1 = all.allocate(sz * coeff_capacity);
		int i = 0;
		for (Iterator begin = Begin(); begin != pos; begin++, i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		all.construct(arr1 + i, value);
		i = last - first;
		pos--;
		Iterator end = End();
		end--;

		for (; end != pos; end--)
		{
			all.construct(arr1 + i, first[i - 1]);
			all.destroy(first + i - 1);
			i--;
		}

		all.deallocate(first, this->end - first);
		first = arr1;
		last = &(first[sz]);
		this->end = &(first[int(sz * coeff_capacity)]);
		end = &(first[i]);
		correct = true;
		return end;
	}
	Iterator         Insert(Iterator pos, unsigned int count, const T& value)
	{
		if (pos.Get_pointer() >= last || pos.Get_pointer() < first)
		{
			correct = false;
			return pos;
		}
		int sz = last - first + count;
		T* arr1 = all.allocate(sz * coeff_capacity);
		int i = 0;
		for (auto it = Begin(); it != pos; it++, i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		int n = i;
		Iterator it = &(arr1[i]);
		for (; i < n + count; i++)
		{
			arr1[i] = value;
		}
		for (; i < last - first + count; i++)
		{
			all.construct(arr1 + i, first[i - count]);
			all.destroy(first + i - count);
		}
		all.deallocate(first, end - first);
		first = arr1;
		last = &(first[sz]);
		this->end = &(first[int(sz * coeff_capacity)]);
		correct = true;
		return it;
	}
	Iterator         Insert(Iterator pos, std::initializer_list<T> ilist)
	{
		if (pos.Get_pointer() >= last || pos.Get_pointer() < first)
		{
			correct = false;
			return pos;
		}
		int sz = last - first + ilist.size();
		T* arr1 = all.allocate(sz * coeff_capacity);
		int i = 0;
		for (auto it = Begin(); it != pos; it++, i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		int n = i;
		Iterator it = &(arr1[i]);
		for (auto j = ilist.begin(); i < n + ilist.size(); i++, j++)
		{
			all.construct(arr1 + i, *j);
		}
		for (; i < sz; i++)
		{
			all.construct(arr1 + i, first[i - ilist.size()]);
			all.destroy(first + i);
		}
		all.deallocate(first, end - first);
		first = arr1;
		last = &(first[sz]);
		end = &(first[int(sz * coeff_capacity)]);
		correct = true;

		return it;
	}
	Iterator		 Erase(Iterator pos) {
		if (pos.Get_pointer() >= last || pos.Get_pointer() < first)
		{
			correct = false;
			return pos;
		}
		int sz = last - first - 1;
		T* arr1 = all.allocate(sz * coeff_capacity);
		int i = 0;
		for (Iterator begin = Begin(); begin != pos; begin++, i++)
		{
			all.construct(arr1 + i, first[i]);
			all.destroy(first + i);
		}
		all.destroy(first + ++i);
		i = last - first;
		Iterator end = End();
		end--;
		pos--;
		for (; end != pos; end--)
		{
			all.construct(arr1 + i - 1, first[i]);
			all.destroy(first + i);
			i--;
		}
		all.deallocate(first, this->end - first);
		first = arr1;
		last = &(first[sz]);
		this->end = &(first[int(sz * coeff_capacity)]);
		end = &(first[i]);
		correct = true;
		return end;
	}
	Iterator         Erase(Iterator fir, Iterator las)
	{
		
		if((fir.Get_pointer() >= last || fir.Get_pointer() < first) || (las.Get_pointer() >= last || las.Get_pointer() < first) || fir > las )
		{
			correct = false;
			return fir;
		}
		int sz = last - first - (las - fir);
		T* arr1 = all.allocate(sz * coeff_capacity);
		int i = 0;
		for (Iterator begin = Begin(); begin != fir; begin++)
		{
			all.construct(arr1 + i, *begin);						
			i++;
		}
		i = last - first - 1;
		Iterator end = End();
		end--;
		las--;

		for (; end != las; end--)
		{
			arr1[i - (las - fir) - 1] = *end;
			i--;
		}
		all.deallocate(first, this->end - first);
		first = arr1;
		last = &first[sz];
		this->end = &first[int(sz * coeff_capacity)];
		end = &first[i - (las - fir)];
		correct = true;
		return end;
	}
	Reverse_iterator RBegin() { return last - 1; }
	Reverse_iterator REnd() { return first; }
	//iterators

	//help method
	void Console()
	{
		if (last - first != 0)
		{
			for (int i = 0; i < last - first; i++)
			{
				cout << first[i] << " ";
			}
			cout << endl;
		}
		else
			cout << "������ ������ ������!";
	}
	//help method

private:
	const float  coeff_capacity = (1 + sqrt(5)) / 2;
	T* first;
	T* last;
	T* end;
	allocator<T> all;
	bool correct = true;
};

