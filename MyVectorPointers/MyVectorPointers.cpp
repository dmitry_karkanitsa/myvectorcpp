﻿#include <iostream>
#include <vector>
#include <memory>
#include "MyVector.h";
using namespace std;

//
void F_out() 
{
	cout << "Набор допустимых команд:" << endl;
	cout << "Vector_out" << endl;
	cout << "Push_back" << endl;
	cout << "Pop_back" << endl;
	cout << "Size" << endl;
	cout << "Empty" << endl;
	cout << "Capacity" << endl;
	cout << "Clear" << endl;
	cout << "Erase" << endl;
	cout << "Assign" << endl;
	cout << "Resize" << endl;
	cout << "Max_size" << endl;
	cout << "At" << endl;
	cout << "Front" << endl;
	cout << "Back" << endl;
	cout << "Shrink_to_fit" << endl;
	cout << "Reserve" << endl;
	cout << "Sort" << endl;
	cout << "Binary_search" << endl;
}
void In_command(string& com)
{
	cout << "Введите команду: ";
	cin >> com;
	cout << endl;
}
void Ch_command(MyVector<int>& v, string& com)
{
	if (com == "Vector_out")
	{
		v.Console();
	}
	else if (com == "Push_back")
	{
		int value;
		cout << "Введите значение: ";
		cin >> value;
		v.Push_back(value);
	}
	else if (com == "Pop_back")
	{
		v.Pop_back();
	}
	else if (com == "Size")
	{
		cout << "Размер вектора = " << v.Size();
	}
	else if (com == "Empty")
	{
		if (v.Empty())
			cout << "Не пуст" << endl;
		else
			cout << "Пуст" << endl;
		
	}
	else if (com == "Capacity")
	{
		cout << "Ёмкость вектора =  " << v.Capacity();
	}
	else if (com == "Clear")
	{
		v.Clear();
	}
	else if (com == "Erase")
	{
		int index;
		cout << "Введите index: ";
		cin >> index;
		v.Erase(index);
	}
	else if (com == "Assign")
	{
		int count;
		int value;
		cout << "Введите новый размер: ";
		cin >> count;
		cout << "Введите значения: ";
		cin >> value;
		v.Assign(count, value);
	}
	else if (com == "Resize")
	{
		int count;
		int value;
		cout << "Введите новый размер: ";
		cin >> count;
		cout << "Введите значения: ";
		cin >> value;
		v.Resize(count, value);
	}
	else if (com == "Max_size")
	{
		cout << "Максимально возможное количество элементов = " << v.Max_Size() << endl;
	}
	else if (com == "At")
	{
		int index;
		cout << "Введите индекс: ";
		cin >> index;
		cout << v.At(index) << endl;
	}
	else if (com == "Front")
	{
		cout << v.Front() << endl;
	}
	else if (com == "Back")
	{
		cout << v.Back() << endl;
	}
	else if (com == "Shrink_to_fit")
	{
		v.Shrink_to_fit();
	}
	else if (com == "Reserve")
	{
		int count;
		cout << "Введите количество элементов: ";
		cin >> count;
		v.Reserve(count);
	}	
	else if (com == "Sort")
	{
		v.Insertion_sort();
	}
	else if (com == "Binary_search")
	{
		int value;
		cout << "Введите значение: ";
		cin >> value;
		cout << "Индекс равен = " << v.Binary_search(value);
	}
	cout << endl<<endl;
}
//
struct person {
	int age;
	int height;
	person& operator << (const person& p) {
		cout << p.age;
		cout << p.height;
	}
};

int main()
{
	setlocale(LC_ALL, "");
	person p1;
	person p2;
	p1.age = 13;
	p1.height = 140;
	p2.height = 140;
	p2.height = 140;
	MyVector<person> v1{p2,p1 };
	cout << v1[0];
	
}
